export default class Process {
    constructor(private config: any) { }
    private regPx: RegExp = /([-]?[\d.]+)p(x)?/;
    private regPxAll: RegExp = /([-]?[\d.]+)px/g;

    convert(text: string) {
        let match = text.match(this.regPx)
        if (!match) return '';
        return this.px2vh(match[1]);
    }

    convertAll(text: string): string {
        if (!text) return text;
        return text.replace(this.regPxAll, (word: string) => {
            const res = this.px2vh(word);
            if (res) return res.vh;
            return word;
        });
    }

    private px2vh(text: string) {
        const pxValue = parseFloat(text);

        let vh: string = +(pxValue / this.config.height * 100).toFixed(this.config.toFixedNum) + 'vh';
        return {
            px: text,
            pxValue: pxValue,
            vh: vh
        }
    }
}