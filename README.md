![https://img.shields.io/badge/vscode-1.25.0-brightgreen.svg](https://img.shields.io/badge/vscode-1.25.0-brightgreen.svg) ![https://img.shields.io/badge/typescript-passing-blue.svg](https://img.shields.io/badge/typescript-passing-blue.svg)

## px2vh

一个 px 单位转成 vh 单位的 VSCode 插件(基于[px2vw扩展插件](https://github.com/liurongqing/px2vw)修改而来)

## 安装

1. git clone代码

1. npm i安装依赖包，全局安装vsce（npm i -g vsce）

1. 在项目目录下执行vsce package

1. 打开vscode -> 扩展 -> 点击三个小点 -> 从VSIX安装 -> 成功后重启/重新加载vscode。


## 使用

- 自动提示
- `cmd + shift + p` 调出命令行全局替换 (mac 快捷键)，输入 `px2vh`

## 支持语言

- html
- vue
- css
- less
- sass
- scss
- stylus

## 配置

根据设计稿高度设置大小(默认：1334)之后就可以直接按设计稿大小编写。

- `px2vh.height` 设计高度
- `px2vh.toFixedNum` 保留小数
